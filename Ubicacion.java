package net.sgoliver.android.localizacion;

/**
 * Created by profe on 19/03/2018.
 */

public class Ubicacion {

    public double latitud;
    public double longitud;
    public long tiempo;

    public Ubicacion()
    {

    }


    public Ubicacion(double latitud, double longitud, long tiempo) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.tiempo = tiempo;
    }
}
